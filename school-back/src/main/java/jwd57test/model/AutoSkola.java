package jwd57test.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class AutoSkola {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true, nullable = false)
	private String nazivSkole;
	
	@Column (nullable = false)
	private int godinaOsnivanja;
	
	@Column
	private int brojVozila;
	
	@OneToMany(mappedBy = "autoSkola")
	private List<Polaznik> polaznici = new ArrayList<Polaznik>();
	
	@OneToMany (mappedBy = "autoSkola")
	private List<Polaganje> polaganja = new ArrayList<Polaganje>();

	public AutoSkola() {
		super();
	}

	public AutoSkola(Long id, String nazivSkole, int godinaOsnivanja, int brojVozila, List<Polaznik> polaznici,
			List<Polaganje> polaganja) {
		super();
		this.id = id;
		this.nazivSkole = nazivSkole;
		this.godinaOsnivanja = godinaOsnivanja;
		this.brojVozila = brojVozila;
		this.polaznici = polaznici;
		this.polaganja = polaganja;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AutoSkola other = (AutoSkola) obj;
		return Objects.equals(id, other.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazivSkole() {
		return nazivSkole;
	}

	public void setNazivSkole(String nazivSkole) {
		this.nazivSkole = nazivSkole;
	}

	public int getGodinaOsnivanja() {
		return godinaOsnivanja;
	}

	public void setGodinaOsnivanja(int godinaOsnivanja) {
		this.godinaOsnivanja = godinaOsnivanja;
	}

	public int getBrojVozila() {
		return brojVozila;
	}

	public void setBrojVozila(int brojVozila) {
		this.brojVozila = brojVozila;
	}

	public List<Polaznik> getPolaznici() {
		return polaznici;
	}

	public void setPolaznici(List<Polaznik> polaznici) {
		this.polaznici = polaznici;
	}

	public List<Polaganje> getPolaganja() {
		return polaganja;
	}

	public void setPolaganja(List<Polaganje> polaganja) {
		this.polaganja = polaganja;
	}

	@Override
	public String toString() {
		return "AutoSkola [id=" + id + ", nazivSkole=" + nazivSkole + ", godinaOsnivanja=" + godinaOsnivanja
				+ ", brojVozila=" + brojVozila + ", polaznici=" + polaznici + ", polaganja=" + polaganja + "]";
	}
	
	
}
