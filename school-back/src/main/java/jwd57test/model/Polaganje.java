package jwd57test.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Polaganje {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private int brojMesta;
	
	@Column(nullable = false)
	private String datum;
	
	@ManyToOne
	private AutoSkola autoSkola;

	public Polaganje() {
		super();
	}

	public Polaganje(Long id, int brojMesta, String datum, AutoSkola autoSkola) {
		super();
		this.id = id;
		this.brojMesta = brojMesta;
		this.datum = datum;
		this.autoSkola = autoSkola;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Polaganje other = (Polaganje) obj;
		return Objects.equals(id, other.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getBrojMesta() {
		return brojMesta;
	}

	public void setBrojMesta(int brojMesta) {
		this.brojMesta = brojMesta;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public AutoSkola getAutoSkola() {
		return autoSkola;
	}

	public void setAutoSkola(AutoSkola autoSkola) {
		this.autoSkola = autoSkola;
	}

	@Override
	public String toString() {
		return "Polaganje [id=" + id + ", brojMesta=" + brojMesta + ", datum=" + datum + ", autoSkola=" + autoSkola
				+ "]";
	}
	
	
	
}
