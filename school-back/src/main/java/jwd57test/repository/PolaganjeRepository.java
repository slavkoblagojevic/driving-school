package jwd57test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd57test.model.Polaganje;

@Repository
public interface PolaganjeRepository extends JpaRepository<Polaganje, Long>{

	List<Polaganje> findAllByAutoSkolaId(Long skolaId);

}
