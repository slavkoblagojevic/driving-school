package jwd57test.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd57test.model.Polaznik;

@Repository
public interface PolaznikRepository extends JpaRepository<Polaznik, Long>{

	List<Polaznik> findAllByImePolaznikaIgnoreCaseContaining(String imePolaznika);

	List<Polaznik> findAllByAutoSkolaIdAndImePolaznikaIgnoreCaseContaining(Long autoSkolaId, String imePolaznika);


	
}
