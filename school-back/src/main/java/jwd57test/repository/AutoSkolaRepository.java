package jwd57test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd57test.model.AutoSkola;

@Repository
public interface AutoSkolaRepository extends JpaRepository<AutoSkola, Long>{
	
}
