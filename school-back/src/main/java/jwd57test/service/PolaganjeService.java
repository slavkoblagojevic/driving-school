package jwd57test.service;

import java.util.List;

import jwd57test.model.Polaganje;

public interface PolaganjeService {

	List<Polaganje> getPolaganja(Long skolaId);

}
