package jwd57test.service;

import jwd57test.model.AutoSkola;
import jwd57test.model.Polaganje;

import java.util.List;

public interface AutoSkolaService {

	List<AutoSkola> getAll();

	AutoSkola getOne(Long id);

}
