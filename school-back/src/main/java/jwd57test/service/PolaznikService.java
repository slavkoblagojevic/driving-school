package jwd57test.service;

import java.util.List;

import org.springframework.stereotype.Service;

import jwd57test.model.Polaznik;

public interface PolaznikService {

	List<Polaznik> findAll();

	Polaznik getOne(Long id);

	Polaznik save(Polaznik polaznik);

	Polaznik update(Polaznik polaznik);

	Polaznik delete(Long id);

	List<Polaznik> findAll(Long autoSkolaId, String imePolaznika);

}
