package jwd57test.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd57test.model.Polaznik;
import jwd57test.repository.PolaznikRepository;
import jwd57test.service.PolaznikService;

@Service
public class JpaPolaznikService implements PolaznikService {

	@Autowired
	PolaznikRepository polaznikRepository;

	@Override
	public List<Polaznik> findAll() {

		return polaznikRepository.findAll();
	}

	@Override
	public Polaznik getOne(Long id) {

		return polaznikRepository.getOne(id);
	}

	@Override
	public Polaznik save(Polaznik polaznik) {

		return polaznikRepository.save(polaznik);
	}

	@Override
	public Polaznik update(Polaznik polaznik) {

		return polaznikRepository.save(polaznik);
	}

	@Override
	@Transactional
	public Polaznik delete(Long id) {

		Optional<Polaznik> optional = polaznikRepository.findById(id);

		if (optional.isPresent()) {

			Polaznik polaznik = optional.get();

			polaznikRepository.deleteById(id);
			return polaznik;
		}

		return null;
	}

	@Override
	public List<Polaznik> findAll(Long autoSkolaId, String imePolaznika) {

		if (imePolaznika == null) {
			imePolaznika = "";
		}

		if (autoSkolaId == null) {
			return polaznikRepository.findAllByImePolaznikaIgnoreCaseContaining(imePolaznika);
		}

		return polaznikRepository.findAllByAutoSkolaIdAndImePolaznikaIgnoreCaseContaining(autoSkolaId, imePolaznika);
	}

}
