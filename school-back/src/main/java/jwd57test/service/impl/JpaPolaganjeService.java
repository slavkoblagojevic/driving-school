package jwd57test.service.impl;

import java.io.Console;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd57test.model.Polaganje;
import jwd57test.repository.PolaganjeRepository;
import jwd57test.service.PolaganjeService;

@Service
public class JpaPolaganjeService implements PolaganjeService {

	@Autowired
	PolaganjeRepository polaganjeRepository;
	
	@Override
	public List<Polaganje> getPolaganja(Long skolaId) {
		
		if(skolaId == null) {
			return polaganjeRepository.findAll();
		}
		
		return polaganjeRepository.findAllByAutoSkolaId(skolaId);
	}

}
