package jwd57test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd57test.model.AutoSkola;
import jwd57test.model.Polaganje;
import jwd57test.repository.AutoSkolaRepository;
import jwd57test.service.AutoSkolaService;

@Service
public class JpaAutoSkolaService implements AutoSkolaService{

	@Autowired
	AutoSkolaRepository autoSkolaRepository;
	
	@Override
	public List<AutoSkola> getAll() {
		
		return autoSkolaRepository.findAll();
	}

	@Override
	public AutoSkola getOne(Long id) {
		
		return autoSkolaRepository.getOne(id);
	}
	
}
