package jwd57test.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd57test.model.Polaznik;
import jwd57test.service.PolaznikService;
import jwd57test.support.PolaznikDTOToPolaznik;
import jwd57test.support.PolaznikToPolaznikDTO;
import jwd57test.web.dto.PolaznikDTO;

@RestController
@RequestMapping(value = "/api/students", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class PolaznikController {

	@Autowired
	PolaznikService polaznikService;

	@Autowired
	PolaznikToPolaznikDTO toPolaznikDTO;

	@Autowired
	PolaznikDTOToPolaznik toPolaznik;

	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<PolaznikDTO>> getAll(
			@RequestParam(required = false) Long schoolId,
			@RequestParam(required = false) String studentName
			) {

		List<Polaznik> polaznici = polaznikService.findAll(schoolId, studentName);

		return new ResponseEntity<>(toPolaznikDTO.convert(polaznici), HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<PolaznikDTO> getOne(@PathVariable Long id) {

		Polaznik polaznik = polaznikService.getOne(id);

		if (polaznik != null) {
			return new ResponseEntity<>(toPolaznikDTO.convert(polaznik), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PolaznikDTO> create(@Valid @RequestBody PolaznikDTO polaznikDTO) {

		Polaznik polaznik = toPolaznik.convert(polaznikDTO);

		Polaznik sacuvaniPolaznik = polaznikService.save(polaznik);

		return new ResponseEntity<>(toPolaznikDTO.convert(sacuvaniPolaznik), HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PolaznikDTO> update(@PathVariable Long id, @Valid @RequestBody PolaznikDTO polaznikDTO) {

		if (!id.equals(polaznikDTO.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Polaznik polaznik = toPolaznik.convert(polaznikDTO);
		//potreban ID da u bazi pronadje polaznika kog menja
		polaznik.setId(id);

		Polaznik sacuvaniPolaznik = polaznikService.update(polaznik);

		return new ResponseEntity<>(toPolaznikDTO.convert(sacuvaniPolaznik), HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<PolaznikDTO> delete(@PathVariable Long id) {

		Polaznik obrisaniPolaznik = polaznikService.delete(id);

		if (obrisaniPolaznik == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>( toPolaznikDTO.convert(obrisaniPolaznik) ,HttpStatus.OK);
		}

	}
}
