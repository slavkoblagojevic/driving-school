package jwd57test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jwd57test.model.AutoSkola;
import jwd57test.model.Polaganje;
import jwd57test.service.AutoSkolaService;
import jwd57test.service.PolaganjeService;
import jwd57test.support.AutoSkolaToAutoSkolaDTO;
import jwd57test.support.PolaganjeToPolaganjeDTO;
import jwd57test.web.dto.AutoSkolaDTO;
import jwd57test.web.dto.PolaganjeDTO;

@RestController
@RequestMapping(value = "/api/schools", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class AutoSkolaController {

	@Autowired
	AutoSkolaService autoSkolaService;
	
	@Autowired
	AutoSkolaToAutoSkolaDTO toSkolaDTO;
	
	@Autowired
	PolaganjeService polaganjeService;
	
	@Autowired
	PolaganjeToPolaganjeDTO toPolaganjeDTO;
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<AutoSkolaDTO>> getAll() {
		
		List<AutoSkola> autoSkole = autoSkolaService.getAll();
		
		return new ResponseEntity<>(toSkolaDTO.convert(autoSkole), HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping("/{schoolId}/exams")
	public ResponseEntity<List<PolaganjeDTO>> getPolaganja(@PathVariable(required = false) Long schoolId) {
	
		List<Polaganje> polaganja = polaganjeService.getPolaganja(schoolId);
		
		return new ResponseEntity<>( toPolaganjeDTO.convert(polaganja) ,HttpStatus.OK);
	}
}
