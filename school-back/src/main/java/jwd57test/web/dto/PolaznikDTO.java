package jwd57test.web.dto;

import javax.validation.constraints.Size;

public class PolaznikDTO {
	
	private Long id;
	
	@Size( max = 30)
	private String studentName;

	private String studentSurname;

	private int birthYear;

	private String place;

	private Long schoolId;
	
	private String schoolName;

	private boolean theoryAttended;

	private boolean drivingAttended;

	private boolean passed;
	
	private boolean examRegistered;

	public PolaznikDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentSurname() {
		return studentSurname;
	}

	public void setStudentSurname(String studentSurname) {
		this.studentSurname = studentSurname;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public boolean isTheoryAttended() {
		return theoryAttended;
	}

	public void setTheoryAttended(boolean theoryAttended) {
		this.theoryAttended = theoryAttended;
	}

	public boolean isDrivingAttended() {
		return drivingAttended;
	}

	public void setDrivingAttended(boolean drivingAttended) {
		this.drivingAttended = drivingAttended;
	}

	public boolean isPassed() {
		return passed;
	}

	public void setPassed(boolean passed) {
		this.passed = passed;
	}

	public boolean isExamRegistered() {
		return examRegistered;
	}

	public void setExamRegistered(boolean examRegistered) {
		this.examRegistered = examRegistered;
	}

	
	
}
