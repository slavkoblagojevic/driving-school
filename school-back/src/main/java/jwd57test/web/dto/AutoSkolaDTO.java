package jwd57test.web.dto;

import javax.validation.constraints.Size;

public class AutoSkolaDTO {

	private Long id;

	@Size( max = 50)
	private String schoolName;

	private int establishmentYear;

	private int vehicleNumber;

	public AutoSkolaDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public int getEstablishmentYear() {
		return establishmentYear;
	}

	public void setEstablishmentYear(int establishmentYear) {
		this.establishmentYear = establishmentYear;
	}

	public int getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(int vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	
}
