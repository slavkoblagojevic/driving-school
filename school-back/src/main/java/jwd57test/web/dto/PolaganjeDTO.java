package jwd57test.web.dto;

import javax.validation.constraints.Positive;

public class PolaganjeDTO {
	
	private Long id;
	
	@Positive (message = "Broj polaganja mora biti pozitivan broj")
	private int placesNumber;

	private String date;

	private Long schoolId;
	
	private String schoolName;

	public PolaganjeDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getPlacesNumber() {
		return placesNumber;
	}

	public void setPlacesNumber(int placesNumber) {
		this.placesNumber = placesNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}


}
