package jwd57test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd57test.model.Polaganje;
import jwd57test.web.dto.PolaganjeDTO;

@Component
public class PolaganjeToPolaganjeDTO implements Converter<Polaganje, PolaganjeDTO> {

	@Override
	public PolaganjeDTO convert(Polaganje polaganje) {
		
		PolaganjeDTO polaganjeDTO = new PolaganjeDTO();
		
		polaganjeDTO.setId(polaganje.getId());
		polaganjeDTO.setDate(polaganje.getDatum());
		polaganjeDTO.setPlacesNumber(polaganje.getBrojMesta());
		polaganjeDTO.setSchoolId(polaganje.getAutoSkola().getId());
		polaganjeDTO.setSchoolName(polaganje.getAutoSkola().getNazivSkole());
		
		return polaganjeDTO;
	}

	public List<PolaganjeDTO> convert (List<Polaganje> polaganja){
		
		List<PolaganjeDTO> polaganjaDTO = new ArrayList<PolaganjeDTO>();
		
		for(Polaganje po : polaganja) {
			polaganjaDTO.add(convert(po));
		}
		
		return polaganjaDTO;
	}
}
