package jwd57test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd57test.model.AutoSkola;
import jwd57test.web.dto.AutoSkolaDTO;

@Component
public class AutoSkolaToAutoSkolaDTO implements Converter<AutoSkola, AutoSkolaDTO> {

	@Override
	public AutoSkolaDTO convert(AutoSkola autoSkola) {

		AutoSkolaDTO autoSkolaDTO = new AutoSkolaDTO();

		autoSkolaDTO.setId(autoSkola.getId());
		autoSkolaDTO.setSchoolName(autoSkola.getNazivSkole());
		autoSkolaDTO.setEstablishmentYear(autoSkola.getGodinaOsnivanja());
		autoSkolaDTO.setVehicleNumber(autoSkola.getBrojVozila());

		return autoSkolaDTO;
	}
	
	public List<AutoSkolaDTO> convert(List<AutoSkola> skole){
		
		List<AutoSkolaDTO> skoleDTO = new ArrayList<AutoSkolaDTO>();
		
		for(AutoSkola as : skole) {
			skoleDTO.add(convert(as));
		}
		
		return skoleDTO;
	}
}

