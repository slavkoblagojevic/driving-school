package jwd57test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd57test.model.Polaznik;
import jwd57test.service.AutoSkolaService;
import jwd57test.web.dto.PolaznikDTO;

@Component
public class PolaznikDTOToPolaznik implements Converter<PolaznikDTO, Polaznik> {

	@Autowired
	AutoSkolaService autoSkolaService;
	
	@Override
	public Polaznik convert(PolaznikDTO polaznikDTO) {
		
		Polaznik polaznik = new Polaznik();
		
		polaznik.setImePolaznika(polaznikDTO.getStudentName());
		polaznik.setPrezimePolaznika(polaznikDTO.getStudentSurname());
		polaznik.setGodinaRodjenja(polaznikDTO.getBirthYear());
		polaznik.setMesto(polaznikDTO.getPlace());
		polaznik.setAutoSkola(autoSkolaService.getOne(polaznikDTO.getSchoolId()));
		polaznik.setOdslusaoTeoriju(polaznikDTO.isTheoryAttended());
		polaznik.setOdradioVoznju(polaznikDTO.isDrivingAttended());
		polaznik.setPolozio(polaznikDTO.isPassed());
		polaznik.setPrijavioPolaganje(polaznikDTO.isExamRegistered());
		
		
		return polaznik;
	}
	
}
