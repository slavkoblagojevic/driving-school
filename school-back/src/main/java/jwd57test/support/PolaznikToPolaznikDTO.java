package jwd57test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd57test.model.Polaznik;
import jwd57test.web.dto.PolaznikDTO;

@Component
public class PolaznikToPolaznikDTO implements Converter<Polaznik, PolaznikDTO> {

	@Override
	public PolaznikDTO convert(Polaznik polaznik) {

		PolaznikDTO polaznikDTO = new PolaznikDTO();

		polaznikDTO.setId(polaznik.getId());
		polaznikDTO.setStudentName(polaznik.getImePolaznika());
		polaznikDTO.setStudentSurname(polaznik.getPrezimePolaznika());
		polaznikDTO.setBirthYear(polaznik.getGodinaRodjenja());
		polaznikDTO.setPlace(polaznik.getMesto());
		polaznikDTO.setSchoolId(polaznik.getAutoSkola().getId());
		polaznikDTO.setSchoolName(polaznik.getAutoSkola().getNazivSkole());
		polaznikDTO.setTheoryAttended(polaznik.isOdslusaoTeoriju());
		polaznikDTO.setDrivingAttended(polaznik.isOdradioVoznju());
		polaznikDTO.setPassed(polaznik.isPolozio());
		polaznikDTO.setExamRegistered(polaznik.isPrijavioPolaganje());

		return polaznikDTO;
	}

	public List<PolaznikDTO> convert(List<Polaznik> polaznici) {

		List<PolaznikDTO> polazniciDTO = new ArrayList<PolaznikDTO>();

		for (Polaznik pl : polaznici) {
			polazniciDTO.add(convert(pl));
		}

		return polazniciDTO;
	}

}
