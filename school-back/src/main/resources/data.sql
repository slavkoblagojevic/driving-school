INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');

              
INSERT INTO auto_skola (id, naziv_skole, godina_osnivanja, broj_vozila) VALUES (1, 'Start-Stop', 2010, 20);
INSERT INTO auto_skola (id, naziv_skole, godina_osnivanja, broj_vozila) VALUES (2, 'Donau', 2011, 10);
INSERT INTO auto_skola (id, naziv_skole, godina_osnivanja, broj_vozila) VALUES (3, 'Golf', 2015, 14);
INSERT INTO auto_skola (id, naziv_skole, godina_osnivanja, broj_vozila) VALUES (4, 'Signal', 2012, 12);
INSERT INTO auto_skola (id, naziv_skole, godina_osnivanja, broj_vozila) VALUES (5, 'Drive', 2012, 12);


INSERT INTO polaznik (id, ime_polaznika, prezime_polaznika, godina_rodjenja, mesto, auto_skola_id, odslusao_teoriju, odradio_voznju, polozio, prijavio_polaganje) 
			VALUES (1, 'Adreja', 'Arsic', 1995, 'Sombor', 2, true, true, true, false);
INSERT INTO polaznik (id, ime_polaznika, prezime_polaznika, godina_rodjenja, mesto, auto_skola_id, odslusao_teoriju, odradio_voznju, polozio, prijavio_polaganje) 
			VALUES (2, 'Marko', 'Mandic', 1965, 'Apatin', 3, true, false, false, false);
INSERT INTO polaznik (id, ime_polaznika, prezime_polaznika, godina_rodjenja, mesto, auto_skola_id, odslusao_teoriju, odradio_voznju, polozio, prijavio_polaganje) 
			VALUES (3, 'Jovan', 'Tadic', 1998, 'Vrbas', 1, false, false, false, false);
INSERT INTO polaznik (id, ime_polaznika, prezime_polaznika, godina_rodjenja, mesto, auto_skola_id, odslusao_teoriju, odradio_voznju, polozio, prijavio_polaganje) 
			VALUES (4, 'Stefan', 'Simic', 1998, 'Stapar', 2, false, false, false, false);
INSERT INTO polaznik (id, ime_polaznika, prezime_polaznika, godina_rodjenja, mesto, auto_skola_id, odslusao_teoriju, odradio_voznju, polozio, prijavio_polaganje) 
			VALUES (5, 'Ivana', 'Jovanovic', 1978, 'Kula', 3, false, false, false, false);
INSERT INTO polaznik (id, ime_polaznika, prezime_polaznika, godina_rodjenja, mesto, auto_skola_id, odslusao_teoriju, odradio_voznju, polozio, prijavio_polaganje) 
			VALUES (6, 'Dejan', 'Mirkovic', 1996, 'Apatin', 4, true, false, false, false);
INSERT INTO polaznik (id, ime_polaznika, prezime_polaznika, godina_rodjenja, mesto, auto_skola_id, odslusao_teoriju, odradio_voznju, polozio, prijavio_polaganje) 
			VALUES (7, 'Marina', 'Tasic', 1990, 'Sombor', 1, false, false, false, false);
			
INSERT INTO polaganje (id, broj_mesta, datum, auto_skola_id) VALUES (1, 10, '06/06/2022', 2);
INSERT INTO polaganje (id, broj_mesta, datum, auto_skola_id) VALUES (2, 5, '08/10/2022', 2);
INSERT INTO polaganje (id, broj_mesta, datum, auto_skola_id) VALUES (3, 8, '01/09/2022', 1);
			

