import React, { useState } from "react";
import { useEffect } from "react";
import TestAxios from "../apis/TestAxios";
import StudentsTable from "../components/StudentsTable";
import Search from "../components/Search";
import NavigationBarLayout from "../layouts/NavigationBarLayout";
import { Card, Button } from "react-bootstrap";
import { withNavigation, withParams } from "../routeconf";

const Students = (props) => {
  const [students, setStudents] = useState([]);

  const [schoolIdSearch, setSchoolIdSearch] = useState("");
  const [studentNameSearch, setStudentNameSearch] = useState("");

  const getStudents = () => {
    const config = {
      params: {
        studentName: studentNameSearch,
        schoolId: schoolIdSearch,
      },
    };

    TestAxios.get("/students", config)
      .then((res) => {
        console.log("students", res.data);
        setStudents(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getStudents();
  }, []);

  const editStudent = (studentId, studentDTO) => {
    TestAxios.put("/students/" + studentId, studentDTO)
      .then((res) => {
        alert("Student edited successfully");
        getStudents();
      })
      .catch((error) => {
        alert("Failure!");
        console.log(error);
      });
  };

  return (
    <NavigationBarLayout>
      <h2>STUDENTS</h2>
      <Card style={{ marginBottom: 20, width: "100%" }}>
        <Card.Header>Search</Card.Header>
        <Card.Body>
          <Search
            schoolIdSearch={schoolIdSearch}
            onSchoolIdChange={setSchoolIdSearch}
            studentNameSearch={studentNameSearch}
            onStudentNameChange={setStudentNameSearch}
            onClickSearch={getStudents}
          />
        </Card.Body>
      </Card>
      <Button
        variant="success"
        style={{ marginBottom: 20 }}
        onClick={() => props.navigate("/add-student")}
      >
        Add New Student
      </Button>
      <StudentsTable studentsList={students} onEditStudent={editStudent} />
    </NavigationBarLayout>
  );
};

export default withNavigation(withParams(Students));
