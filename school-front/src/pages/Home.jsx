import React from "react";
import NavigationBarLayout from "../layouts/NavigationBarLayout";

const Home = () => {
  return (
    <NavigationBarLayout>
      <div className="jumbotron jumbotron-fluid m-5">
        <div className="container">
          <h1 className="display-4">DRIVING SCHOOL</h1>
          <p className="lead">WELCOME</p>
        </div>
      </div>
    </NavigationBarLayout>
  );
};

export default Home;
