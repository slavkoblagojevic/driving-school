import React, { useState, useEffect } from "react";
import NavigationBarLayout from "../layouts/NavigationBarLayout";
import { Form, Button, Col, Row, Card } from "react-bootstrap";
import TestAxios from "../apis/TestAxios";
import { withNavigation, withParams } from "../routeconf";

const AddStudent = (props) => {
  const [schools, setSchools] = useState([]);

  const [studentName, setStudentName] = useState("");
  const [studentSurname, setStudentSurname] = useState("");
  const [birthYear, setBirthYear] = useState("");
  const [place, setPlace] = useState("");
  const [schoolId, setSchoolId] = useState("");

  const getSchools = () => {
    TestAxios.get("/schools")
      .then((res) => {
        console.log("schools", res.data);
        setSchools(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getSchools();
  }, []);

  const create = () => {
    const params = {
      studentName: studentName,
      studentSurname: studentSurname,
      birthYear: birthYear,
      place: place,
    };

    if (schoolId !== "") {
      params.schoolId = schoolId;
    } else {
      alert("Choose Driving School!");
    }

    TestAxios.post("/students", params)
      .then((res) => {
        alert("Student successfully added.");
        props.navigate("/students");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const schoolsOption = schools.map((school) => {
    return (
      <option key={school.id} value={school.id}>
        {school.schoolName}
      </option>
    );
  });

  return (
    <NavigationBarLayout>
      <Card>
        <Card.Header>NEW STUDENT</Card.Header>
        <Card.Body>
          <Form.Group className="mb-3">
            <Form.Label>First Name:</Form.Label>
            <Form.Control
              type="text"
              placeholder="First Name"
              value={studentName}
              onChange={(e) => setStudentName(e.target.value)}
            ></Form.Control>
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Last Name:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Last Name"
              value={studentSurname}
              onChange={(e) => setStudentSurname(e.target.value)}
            ></Form.Control>
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Birth Year:</Form.Label>
            <Form.Control
              type="number"
              placeholder="Birth Year"
              value={birthYear}
              onChange={(e) => setBirthYear(e.target.value)}
            ></Form.Control>
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Place:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Place"
              value={place}
              onChange={(e) => setPlace(e.target.value)}
            ></Form.Control>
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Driving School:</Form.Label>
            <Form.Control
              as="select"
              onChange={(event) => setSchoolId(event.target.value)}
            >
              <option value={""}>--Choose School--</option>
              {schoolsOption}
            </Form.Control>
          </Form.Group>
          <Row>
            <Col>
              <Button onClick={create}>Create</Button>
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </NavigationBarLayout>
  );
};

export default withNavigation(withParams(AddStudent));
