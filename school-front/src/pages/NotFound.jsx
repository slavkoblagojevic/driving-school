import React from "react";
import NavigationBarLayout from "../layouts/NavigationBarLayout";

const NotFound = () => {
  return (
    <NavigationBarLayout>
      <h1>404 - Page not found</h1>;
    </NavigationBarLayout>
  );
};

export default NotFound;
