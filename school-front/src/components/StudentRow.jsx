import React from "react";
import { Button } from "react-bootstrap";

const StudentRow = (props) => {
  const { student, onEditStudent } = props;

  const deleteStudent = () => {
    this.delete(student.id);
  };

  const editStudentOdslusaoTeoriju = () =>
    onEditStudent(student.id, {
      ...student,
      theoryAttended: true,
    });
  const editStudentOdradioVoznju = () =>
    onEditStudent(student.id, {
      ...student,
      drivingAttended: true,
    });

  const editStudentPolozio = () =>
    onEditStudent(student.id, {
      ...student,
      passed: true,
    });

  const editStudentPrijavioPolaganje = () =>
    onEditStudent(student.id, {
      ...student,
      examRegistered: true,
    });
  return (
    <tr key={student.id}>
      <td>{student.studentName}</td>
      <td>{student.studentSurname}</td>
      <td>{student.birthYear}</td>
      <td>{student.place}</td>
      <td>{student.schoolName}</td>
      <td>{student.passed === true ? "Yes" : "No"}</td>
      <td>
        {window.localStorage["jwt"] ? (
          <>
            <Button
              variant="danger"
              style={{ marginRight: 5 }}
              onClick={deleteStudent}
            >
              Delete
            </Button>
            {student.theoryAttended === false ? (
              <Button variant="warning" onClick={editStudentOdslusaoTeoriju}>
                Theory Attended
              </Button>
            ) : null}
            {student.theoryAttended && !student.drivingAttended ? (
              <Button
                variant="info"
                style={{ marginRight: 5 }}
                onClick={editStudentOdradioVoznju}
              >
                Driving Attended
              </Button>
            ) : null}
            {student.examRegistered &&
            student.drivingAttended &&
            !student.passed ? (
              <Button
                variant="success"
                style={{ marginRight: 5 }}
                onClick={editStudentPolozio}
              >
                Passed
              </Button>
            ) : null}
            {student.theoryAttended &&
            student.drivingAttended &&
            !student.examRegistered ? (
              <Button
                variant="primary"
                style={{ marginRight: 5 }}
                onClick={editStudentPrijavioPolaganje}
              >
                Exam Registration
              </Button>
            ) : null}
          </>
        ) : null}
      </td>
    </tr>
  );
};

export default StudentRow;
