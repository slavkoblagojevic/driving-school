import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import TestAxios from "../apis/TestAxios";

const Search = (props) => {
  const [schools, setSchools] = useState([]);

  const getSchools = () => {
    TestAxios.get("/schools")
      .then((res) => {
        console.log("schools", res.data);
        setSchools(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getSchools();
  }, []);

  const schoolsOption = schools.map((school) => {
    return (
      <option key={school.id} value={school.id}>
        {school.schoolName}
      </option>
    );
  });

  return (
    <Form>
      <Form.Group>
        <Form.Label>Name:</Form.Label>
        <Form.Control
          type="text"
          placeholder="Name"
          style={{ marginBottom: 10 }}
          value={props.studentNameSearch}
          onChange={(e) => props.onStudentNameChange(e.target.value)}
        ></Form.Control>
      </Form.Group>
      <Form.Group>
        <Form.Label>Driving School:</Form.Label>
        <Form.Control
          as="select"
          style={{ marginBottom: 10 }}
          onChange={(e) => props.onSchoolIdChange(e.target.value)}
        >
          <option value={""}>--Select School--</option>
          {schoolsOption}
        </Form.Control>
      </Form.Group>
      <Button onClick={() => props.onClickSearch()}>Search</Button>
    </Form>
  );
};

export default Search;
