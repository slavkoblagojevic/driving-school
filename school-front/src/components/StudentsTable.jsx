import React from "react";
import StudentRow from "./StudentRow";
import { Table } from "react-bootstrap";

const StudentsTable = (props) => {
  const studentRowsToRender = props.studentsList.map((student) => {
    return <StudentRow student={student} onEditStudent={props.onEditStudent} />;
  });
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Birth Year</th>
          <th>Place</th>
          <th>School</th>
          <th>Passed</th>
          <th></th>
        </tr>
      </thead>
      <tbody>{studentRowsToRender}</tbody>
    </Table>
  );
};

export default StudentsTable;
