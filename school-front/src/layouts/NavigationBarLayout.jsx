import React from "react";
import { Button, Navbar, Nav, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { logout } from "../services/auth";

const NavigationBarLayout = (props) => {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Navbar.Brand as={Link} to="/">
          JWD
        </Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link as={Link} to="/">
            Home
          </Nav.Link>
          <Nav.Link as={Link} to="/students">
            Students
          </Nav.Link>
          <Nav.Link as={Link} to="/add-student">
            Add Student
          </Nav.Link>
        </Nav>
        {window.localStorage["jwt"] ? (
          <Button onClick={() => logout()}>Log out</Button>
        ) : (
          <Button as={Link} to="/login">
            Log in
          </Button>
        )}
      </Navbar>
      <Container>{props.children}</Container>
    </>
  );
};

export default NavigationBarLayout;
