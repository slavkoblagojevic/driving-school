## Driving School

The application provides work with entities: Driving school, Candidates and Exam. It provides the creation of a new student in the driving school.

Project implemented on the final course test "Java Web Development" (FTN Informatika). Application is based on SpringBoot, REST API, ReactJS and Bootstrap.
